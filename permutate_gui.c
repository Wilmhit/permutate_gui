#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <gtk/gtk.h>
#include <stdbool.h>
#define MAX_LENGTH 50 //not more than int8_t max = 127
#define MAX_INT8_T 127

//Entrypoint functions for threads
void ui_entry(void* values);
void worker_entry();

//Structs used to pass data to threads
struct worker_values {
    /*State may have values:
     * -1 if not started.
     *  0 if paused
     *  1 if running
     *  2 if exiting */
    int8_t state;
    int8_t n;
    int n_generated;
};

struct ui_values {
    int8_t progress;
    int8_t state; //see worker_values
    int elapsed_time;
    int estimated_time;
    int8_t n;
    int argc;
    char** argv;
};

////////////////////
//MASTER SECTION
////////////////////

int8_t get_progress(int calculated, int64_t goal);
int64_t get_factorial(int number);
int get_elapsed_time(int start_time);
int get_estimated_time(int start_time, int calculated, int64_t goal);

int main(int argc, char** argv) {
    /* Entrypoint for master thread. Before master */

    //Variables shared by threads
    struct ui_values ui_args;
    struct worker_values worker_args;

    ui_args.progress = 0;
    ui_args.elapsed_time = 0;
    ui_args.estimated_time = 0;
    ui_args.argc = argc;
    ui_args.argv = argv;
    ui_args.state = -1;
    ui_args.n = 0;

    worker_args.state = -1;
    worker_args.n_generated = 0;
    worker_args.n = 0;

    //Creating threads and activating them
    pthread_t ui_thread;
    pthread_t worker_thread;
    pthread_create(&ui_thread, NULL, (void*)ui_entry, &ui_args);
    pthread_create(&worker_thread, NULL, (void*)worker_entry, &worker_args);

    //MASTER BEGINS WORK

    //Wait until start was first pressed
    while (ui_args.state == -1) usleep(1000);
    
    int64_t goal = get_factorial(ui_args.n);
    int start_time = (int) time(NULL);
    
    //Get data from worker, calculate times, pass to gui
    while(ui_args.state != 2) {
	usleep(1000); // for good cpu usage (0.1s)
	worker_args.n = ui_args.n;
	worker_args.state = ui_args.state;
	ui_args.progress = get_progress(worker_args.n_generated, goal);
	ui_args.elapsed_time = get_elapsed_time(start_time);
	ui_args.estimated_time = get_estimated_time(start_time, worker_args.n_generated, goal);
    }

    //Pass exit signal to worker thread
    worker_args.state = 2;

    //Wait for threads and exit
    pthread_join(ui_thread, NULL);
    pthread_join(worker_thread, NULL);
    return 0;
}

int64_t get_factorial(int number) {
    //stackoverflow has you covered
    return tgamma(number + 1);
}

int get_elapsed_time(int start_time) {
    return (int) time(NULL) - start_time;
}

int get_estimated_time(int start_time, int calculated, int64_t goal) {
    if (calculated == 0) return 0;
    double progress = (double) calculated / (double) goal;
    double total_time = 1.0/progress * (double) get_elapsed_time(start_time);
    return (int) total_time - get_elapsed_time(start_time);
}

int8_t get_progress(int calculated, int64_t goal) {
    return (int8_t) ((double) calculated * 100.0/ (double) goal);
}



////////////////////
//GUI SECTION
////////////////////

//Functions used only by gui
static void button_action(GtkWidget* button, gpointer data);
static void exit_action(GtkWidget* button, gpointer data);
gboolean update_field(gpointer f_data);
char* make_text(char* str, int val);
gboolean update_progress(gpointer f_data);
gboolean update_estimated(gpointer f_data);
gboolean update_elapsed(gpointer f_data);

struct field_data { //used for passing data to field update functions
    GtkWidget* widget;
    int* value;
};

struct button_data { //used for passing data to button
    GtkWidget* get_from;
    int8_t* set_to;
    int8_t* state;
};

void ui_entry(void* values) {
    /* Gui build with Gtk 3 */

    struct ui_values* shared = values;
    GtkWidget *window, *grid, *start_button, *progress_label, *estimated_time_label, *elapsed_time_label, *n_entry;
    gtk_init(&shared->argc, &shared->argv);

    /*CREATING WINDOW AND FILLING IT*/

    //creating window
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_container_set_border_width((GtkContainer*) window, (guint) 20);
    gtk_window_set_resizable(GTK_WINDOW(window), false);
    g_signal_connect(window, "destroy", G_CALLBACK(exit_action), &shared->state);

    //creating grid
    grid = gtk_grid_new();
    gtk_grid_set_column_spacing((GtkGrid*) grid, (guint) 10);
    gtk_container_add(GTK_CONTAINER(window), grid);

    //Creating n entry
    n_entry = gtk_entry_new();

    //creating start_button
    start_button = gtk_button_new_with_label("Start/Pause"); //TODO zminić label
    struct button_data callback_data;
    callback_data.get_from = n_entry;
    callback_data.set_to = &shared->n;
    callback_data.state = &shared->state;
    g_signal_connect(start_button, "clicked", G_CALLBACK(button_action), &callback_data);

    //creating labels
    progress_label = gtk_label_new("Progress: 00%"); //TODO zmienić label'e
    estimated_time_label = gtk_label_new("Estimated time left: 00s");
    elapsed_time_label = gtk_label_new("Elapsed time: 00s");
    
    //TODO pierwsze 2 liczby (3 i 4 argument) odpowiadają za położenie widgetu w okienku
    //(kolumna/rząd). Można to sobie zcustomizować.
    gtk_grid_attach(GTK_GRID(grid), n_entry, 0,0,1,1);
    gtk_grid_attach(GTK_GRID(grid), start_button, 1,0,1,1);
    gtk_grid_attach(GTK_GRID(grid), progress_label, 0,1,1,1);
    gtk_grid_attach(GTK_GRID(grid), estimated_time_label, 1,1,1,1);
    gtk_grid_attach(GTK_GRID(grid), elapsed_time_label, 1,2,1,1);

    //show all widgets
    //shared->n = 4;
    gtk_widget_show_all(window);

    /*UI UPDATES*/

    //creating data to pass into callbacks
    struct field_data progress_data;
    struct field_data elapsed_data;
    struct field_data estimated_data;

    //filling data for callbacks
    progress_data.widget = progress_label;
    elapsed_data.widget = elapsed_time_label;
    estimated_data.widget = estimated_time_label;
    progress_data.value = (int*) &shared->progress;
    elapsed_data.value = &shared->elapsed_time;
    estimated_data.value = &shared->estimated_time;

    //passing callbacks into main ui loop with data as arg
    g_timeout_add(100, update_progress, &progress_data);
    g_timeout_add(100, update_elapsed, &elapsed_data);
    g_timeout_add(1000, update_estimated, &estimated_data);
    gtk_main();
    
}
static void exit_action(GtkWidget* button, gpointer data) {
    /* Changes state to 2 and exits gui */
    int8_t* state = (int8_t*) data;
    *state = 2;
    gtk_main_quit();
}

static void button_action(GtkWidget* button, gpointer data){
    /* Changes state between 0/1. If state is -1 changes to 1 and gets n from user.*/
    struct button_data* callback_data = data;
    if (*callback_data->state == -1) {
	//set n to text
	char* from_user = (char*) gtk_entry_get_text(GTK_ENTRY(callback_data->get_from));
	*callback_data->set_to = atoi(from_user);
    }
    if (*callback_data->state == 1) *callback_data->state = 0;
    else *callback_data->state = 1;
}

// Field update functions

gboolean update_progress(gpointer f_data) {
    struct field_data *data = f_data;
    char to_display[25];
    sprintf(to_display, "Progress: %d%%", (int8_t) *data->value); //TODO zmienić label
    gtk_label_set_text(GTK_LABEL(data->widget), (gchar*) to_display);
    return G_SOURCE_CONTINUE;
}

gboolean update_estimated(gpointer f_data) {
    struct field_data *data = f_data;
    char to_display[25];
    sprintf(to_display, "Estimated time: %ds", *data->value); //TODO zmienić label
    gtk_label_set_text(GTK_LABEL(data->widget), (gchar*) to_display);
    return G_SOURCE_CONTINUE;
}

gboolean update_elapsed(gpointer f_data) {
    struct field_data *data = f_data;
    char to_display[25];
    sprintf(to_display, "Elapsed time: %ds", *data->value); //TODO zmienić label
    gtk_label_set_text(GTK_LABEL(data->widget), (gchar*) to_display);
    return G_SOURCE_CONTINUE;
}





////////////////////
//WORKER SECTION
////////////////////

int8_t find_greatest_mobile(int8_t* table,bool* arrows,int8_t length);
int8_t move_k(int8_t* table, bool* arrows, int8_t k_index);
void filp_arrows(int8_t* table, bool* arrows, int8_t length, int8_t k_index);
void save_array(int8_t* table, int8_t length);
void swap_int8(int8_t *i, int8_t *j);
void swap_bool(bool *i, bool *j);

//TODO jeśli decydujemy się na wariant 2 to ta funkcja musi tylko zwiększać
//shared->n_generated, robić sleep() oraz reagować na zmianę shared->state.
void worker_entry(void* values) {
    /* Worker uses Johnson Trotter algorithm to generate permutations. Google it*/
    struct worker_values* shared = values;

    //Set up
    int8_t table[MAX_LENGTH];
    bool arrows[MAX_LENGTH] = {false}; // false points to left; true points to right
    
    //Wait until user selects n
    while (shared->state == -1) usleep(1000);

    //Fill table with numbers 0 to n
    for (int i=0; i != shared->n; i++) {
	table[i] = i;
    }

    //start algorithm
    while (true) {
	while (shared->state == 0) usleep(1000); //if paused wait
	if (shared->state == 2) break; //gui exit
	shared->n_generated += 1;
	save_array(table, shared->n);
	int8_t k_index = find_greatest_mobile(table, arrows, shared->n);
	if (k_index == -1) break; // No more permutations
	k_index = move_k(table, arrows, k_index);
	filp_arrows(table, arrows, shared->n, k_index);
    }
}

int8_t find_greatest_mobile(int8_t* table,bool* arrows,int8_t length){
    /* Finds gretest mobile number in array and returns it's index.
     * Returns -1 if threre is no mobile numbers in array. */
    int8_t k_index = -1;
    for (int8_t i=0; i != length; i++) {
	int8_t* number = &table[i];
	int8_t* pointing_to;

	if (!arrows[i] && i != 0) pointing_to = &table[i-1];
	else if (arrows[i] && i != length-1) pointing_to = &table[i+1];
	else pointing_to = NULL;

	if (pointing_to != NULL && *number > *pointing_to && *number) {
	    if (k_index == -1) k_index = i;
	    else if(table[k_index] < *number) k_index = i;
	}
    }
    return k_index;
}

int8_t move_k(int8_t* table, bool* arrows, int8_t k_index){
    /* Swaps number in array(k) with number it's pointing torwards*/
    int8_t k_pointing_to;
    if (arrows[k_index]) k_pointing_to = k_index + 1;
    else k_pointing_to = k_index - 1;
    swap_int8(&table[k_index], &table[k_pointing_to]);
    swap_bool(&arrows[k_index], &arrows[k_pointing_to]);
    return k_pointing_to;
}

void filp_arrows(int8_t* table, bool* arrows, int8_t length, int8_t k_index){
    /* Flips arrows of all numbers in array greater than number with k_index*/
    int8_t base_value = table[k_index];
    for (int8_t i = 0; i!=length; i++) {
	if (table[i] > base_value) {
	    if (arrows[i] == true) arrows[i] = false;
	    else arrows[i] = true;
	}
    }
}

void save_array(int8_t* table, int8_t length) {
    /* Saves array. (Permutation is ready)*/
    printf("[");
    for (int8_t i = 0; i!=(length -1); i++) {
	printf("%d,", table[i]);
    }
    printf("%d", table[length-1]);
    printf("]\n");
}

void swap_int8(int8_t *i, int8_t *j) {
    /* Swaps 2 int8_t inplace*/
   int8_t t = *i;
   *i = *j;
   *j = t;
}

void swap_bool(bool *i, bool *j) {
    /* Swaps 2 bools inplace*/
   bool t = *i;
   *i = *j;
   *j = t;
}
